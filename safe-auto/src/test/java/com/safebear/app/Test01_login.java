package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_login extends BaseTest {
    @Test
    public void testLogin() {
        //step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //step 2 click on the Login link and the Login Page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        //step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }
}
